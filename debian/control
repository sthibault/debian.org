Source: debian.org
Section: local/admin
Priority: required
Maintainer: Debian sysadmin Team <dsa@debian.org>
Uploaders: Peter Palfrader <weasel@debian.org>, Martin Zobel-Helas <zobel@debian.org>, Stephen Gran <sgran@debian.org>, Luca Filipozzi <lfilipoz@debian.org>
Build-Depends: debhelper (>= 10)
Standards-Version: 3.8.1

Package: debian.org
Architecture: all
Depends: userdir-ldap, zsh, tcsh, mksh, ksh, csh, locales-all, exim4-daemon-heavy | postfix, sudo, libpam-pwdfile, vim, nano, munin-node, gnupg, bzip2, cacert-spi | ca-certificates (>> 20080411), ed, cron, dialog, libterm-readline-gnu-perl, logrotate, syslog-ng (>= 3.1), nagios-plugins-standard (>= 1.4.15) | nagios-plugins-basic
Recommends: debian.org-recommended
Conflicts: dchroot-dsa (<= 2:1)
Description: DSA's host metapackage
 This package depends on packages that should be installed on every
 debian.org host, and conflicts with packages that should never be
 installed.

Package: debian.org-recommended-linux
Architecture: all
Depends: vlan, ifenslave, bridge-utils, iproute2,
	strace,
	lsscsi,
	ethtool,
	bwm-ng,
	iputils-tracepath,
	traceroute,
	lshw,
	lsof,
	sysstat
Description: metapackage to pull in recommended, but not essential tools for linux system
 This package depends on packages that are recommended to  be installed on
 every Linux debian.org host but are not essential.

Package: debian.org-recommended
Architecture: any
Depends: debian.org,
	busybox-static,
	debian.org-recommended-linux [linux-any],
	molly-guard
	,
	ruby-msgpack
	,
	man-db
	,
	devscripts
	,
	subversion, rcs, git, bzr | brz,
		git-svn,
		subversion-tools,
		mercurial,
		cvs
	,
	mtr-tiny, tcpdump, tcptraceroute, ndisc6,
	bind9-host, dnsutils, whois, unbound-host
	,
	emacs24-nox | emacs24 | emacs23-nox | emacs23 | emacs22-nox | emacs22 | emacs21-nox | emacs21,
	vim-scripts, vim-addon-manager
	,
	at, uptimed,
	puppet,
		ruby-augeas | libaugeas-ruby,
		augeas-lenses (>= 0.7),
	dlocate
	,
	dsa-nagios-checks
	,
	munin-plugins-extra
	,
	dmidecode [amd64 i386 ia64],
	edac-utils,
	sg3-utils
	,
	ipython, ipython3
	,
	htop, psmisc,
	,
	ncdu,
	mc,
		genisoimage,
	less,
	rsync,
	screen,
	tmux,
	ldap-utils, ldapvi,
	mmv,
	moreutils,
	patchutils,
	diffstat,
	bc,
	netcat-openbsd, socat, telnet
	,
	deborphan
	,
	file
	,
	buffer
	,
	bsd-mailx
	,
	timeout | coreutils (>= 8.0-1)
	,
	wget, curl, lftp, ftp,
	,
	dput
	,
	uuid-runtime
	,
	finger
	,
	xz-utils,
	pigz,
	unzip
	,
	w3m,
	elinks,
	mutt
	,
	ccze,
	multitail
Description: metapackage to pull in recommended, but not essential tools
 This package depends on packages that are recommended to  be installed on
 every debian.org host but are not essential.

Package: debian.org-bugs.debian.org
Architecture: all
Depends: graphviz,
	libcgi-simple-perl,
	libgravatar-url-perl,
	libio-stringy-perl,
	liblist-moreutils-perl,
	libmail-rfc822-address-perl,
	libmime-tools-perl,
	libmldbm-perl,
	libparams-validate-perl,
	libsafe-hole-perl,
	libtext-template-perl,
	liburi-perl,
	libhtml-tree-perl,
	libsoap-lite-perl,
	libtext-iconv-perl,
	libdbd-pg-perl,
	libfile-libmagic-perl,
	libnet-dns-sec-perl,
	libwww-perl,
	imagemagick,
	libdatetime-format-mail-perl,
	libdatetime-format-pg-perl,
	libdbix-class-perl,
	libdbix-class-schema-loader-perl,
	libdbix-class-helpers-perl,
    libdbix-class-deploymenthandler-perl,
    libdbix-class-timestamp-perl,
    libterm-progressbar-perl,
    libtext-xslate-perl,
	python-pyinotify,
	python-yaml,
# contributors.debian.org's data mining
	python3-requests,
	python3-debian,
	python3-six
Description: metapackages for bugs.debian.org dependencies
 This package depends on everything that is needed for a bugs.debian.org
 mirror or (in the future) bugs-master.

Package: debian.org-bugs.debian.org-master
Architecture: all
Depends: debian.org-bugs.debian.org,
	gnuplot,
    libclamav-client-perl,
	ttf-liberation,
	kgb-client-git,
	lynx	
Description: metapackages for bugs-master.debian.org dependencies
 This package depends on everything that is needed for a
 bugs-master.debian.org

Package: debian.org-bugs.debian.org-search
Architecture: all
Depends: debian.org-bugs.debian.org,
	hyperestraier,
	libdate-manip-perl,
	libsearch-estraier-perl
Description: metapackages for bugs-search.debian.org dependencies
 This package depends on everything that is needed for a
 bugs-search.debian.org

Package: debian.org-bugs.debian.org-mx
Architecture: all
Depends: debian.org-bugs.debian.org,
	spamassassin,
	build-essential,
	spamc,
	libmail-spf-perl,
	re2c,
	libsys-syslog-perl,
	libio-socket-inet6-perl,
	libnet-ident-perl,
	libdbi-perl,
	pyzor,
	libmail-dkim-perl,
	libdbd-pg-perl
Description: metapackages for bugs.debian.org mail exchanger
 This package depends on everything that is needed for a bugs.debian.org
 MX.


Package: debian.org-packages-master.debian.org
Architecture: all
Depends: debian.org-packages.debian.org,
	procmail,
	gettext,
	libhtml-template-perl,
	libxml-rss-perl,
	libtimedate-perl,
	libio-stringy-perl,
	freecdb,
	tig
Description: metapackages for packages.debian.org dependencies
 This package depends on everything that is needed for a
 packages-master.debian.org server.  For the mirrors all you need is the
 debian.org-packages.debian.org package.

Package: debian.org-packages.debian.org
Architecture: all
Depends: liblocale-maketext-lexicon-perl,
	libi18n-acceptlanguage-perl,
	libnumber-format-perl,
	libcompress-zlib-perl,
	libhtml-parser-perl,
	libmldbm-perl,
	libtext-iconv-perl,
	libclass-accessor-perl,
	liburi-perl,
	libtemplate-perl,
	liblingua-stem-perl,
	libsearch-xapian-perl,
	libtimedate-perl,
	libcgi-fast-perl,
	libapache2-mod-fcgid,
	dpkg-dev,
	rsync
Description: metapackages for packages.debian.org dependencies
 This package depends on everything that is needed for a packages.debian.org
 mirror.  For packages-master install the debian.org-packages-master.debian.org
 package.

Package: debian.org-www-master.debian.org
Architecture: all
Depends: debiandoc-sgml,
	asciidoc,
	cm-super,
	debhelper,
	docbook,
	docbook-dsssl,
	docbook-utils,
	docbook-xsl,
	dpkg-dev,
	fonts-arphic-uming,
	fonts-mlym,
	gawk (>= 1:4.0.1),
	gettext,
	ghostscript,
	imagemagick,
	iso-codes,
	isoquery,
	jadetex,
	kcc,
	ko.tex-base,
	latex-cjk-all,
	latex-cjk-chinese,
	ldap-utils,
	libcgi-pm-perl,
	libemail-address-perl,
	libintl-perl,
	libmime-lite-perl,
	libnet-ldap-perl,
	libsoap-lite-perl,
	libtimedate-perl,
	libwww-perl,
	libxml2-utils,
	libxml-rss-perl,
	locales, locales-all,
	lmodern,
	maildrop,
	opencc,
	openjade | openjade1.3,
	opensp,
	po4a,
	poxml,
	procmail,
	texlive-fonts-recommended,
	texlive-lang-all,
	latex-cjk-japanese,
	texlive-latex-base,
	texlive-latex-extra,
	texlive-latex-recommended,
	fonts-wqy-microhei,
	fonts-vlgothic,
	tidy,
	time,
	uni2ascii,
	wml,
	xmlroff, dblatex,
	xplanet,
	xplanet-images,
	xsltproc,
	libjson-perl,
Description: metapackages for www-master.debian.org dependencies
 This package depends on everything that is needed for a www-master.debian.org.


Package: debian.org-buildd.debian.org
Architecture: all
Depends:
	dose-builddebcheck,
	dose-distcheck,
	ghostscript,
	libcompress-bzip2-perl,
	libcompress-zlib-perl,
	libhash-merge-perl,
	libmailtools-perl,
	libdbi-perl,
	libdbd-pg-perl,
	libfile-spec-perl,
	libstring-escape-perl,
	libstring-format-perl,
	libyaml-tiny-perl,
	python-apt,
	python-contract,
	python-coverage,
	python-debian,
	python-psycopg2,
	python-mako,
	python-nose,
	python-sqlalchemy,
	python3-coverage,
	python3-debian,
	python3-nose,
	python3-psycopg2,
	python3-sqlalchemy,
	r-base-core,
	r-cran-lattice,
	moreutils (>= 0.39~),
	libdpkg-perl,
	php,
	php-bz2,
	php-cgi,
	php-pgsql,
	libapache2-mod-fcgid,
	libyaml-libyaml-perl
Description: metapackages for buildd.debian.org dependencies
 This package depends on everything that is needed for a buildd.debian.org.


Package: debian.org-patch-tracker.debian.org
Architecture: all
Depends: patchutils,
	python,
	python-cheetah,
	python-pygments,
	python-debian,
	reprepro,
	python-wsgiref,
	python-simplejson,
	python-pysqlite2,
	diffstat
Description: metapackages for patch-tracker.debian.org dependencies
 This package depends on everything that is needed for a patch-tracker.debian.org.

Package: debian.org-planet.debian.org
Architecture: all
Depends: libwww-perl,
	timeout | coreutils (>= 8.0-1),
	debianutils,
	python-django,
	python,
	libdbi-perl, libdate-manip-perl, libdbd-sqlite3-perl
Description: metapackages for planet.debian.org dependencies
 This package depends on everything that is needed for a planet.debian.org.

Package: debian.org-security-tracker.debian.org
Architecture: all
Depends: libdbd-sqlite3-perl,
	sqlite3,
	python-apsw,
	libterm-readline-gnu-perl,
	python-apt,
	python-soappy
Description: metapackages for security-tracker.debian.org dependencies
 This package depends on everything that is needed for a security-tracker.debian.org.


Package: debian.org-ftpmaster.debian.org-code-signing
Architecture: all
Depends: devscripts,
	dpkg-dev,
	dput,
	libengine-pkcs11-openssl (>= 0.4.9-3) | libengine-pkcs11-openssl1.1,
	pesign,
	python3-requests,
	python3-sqlalchemy,
	python3-yaml,
# Needed for sign-file, any kbuild will do
	linux-kbuild-4.9,
Description: metapackage for the code-signing service
 Currently co-located with ftpmaster, this service handles signing of kernels,
 modules, and UEFI executables to support Debian's participation in the UEFI
 Secure Boot programme.

Package: debian.org-ftpmaster.debian.org
Architecture: all
Depends: apt-utils,
	libapt-pkg-dev,
	libemail-sender-perl,
	libcgi-pm-perl,
	python-apt,
	python-debianbts (>= 1.4~bpo50+1),
	lintian,
	python,
	python-debian,
	python-ldap,
	python-psycopg2,
	python-pygresql,
	python-pyrss2gen,
	python-yaml,
	python-rrdtool,
	python-sqlalchemy,
	python-tabulate,
	python3-voluptuous,
	python3-yaml,
	binutils-multiarch,
	build-essential,
	graphviz,
	ikiwiki,
	python-epydoc,
	python-numpy,
	python-soappy,
	r-base,
	symlinks,
	python-magic,
	unp,
	procmail,
	cvs,
	ruby,
	lynx,
	libxml-rss-perl,
	ghostscript,
	perforate,
	wdiff,
	ruby-soap4r,
	ack-grep,
	jdupes,
	python3-debiancontributors,
	python3-psycopg2,
	libgfshare-bin,
	pinentry-curses,
	debian.org-ftpmaster.debian.org-code-signing,
Description: metapackage for ftpmaster
 This package depends on everything that is needed for an ftpmaster setup.

Package: debian.org-ftpmaster.debian.org-poneys
Architecture: all
Depends: bicyclerepair,
	dpkg-dev-el,
	emacs-goodies-el,
	pychecker,
	pylint,
	python-mode,
	gitstats,
	colordiff
Description: metapackage for ftpmaster extras
 Things not really needed needed.

Package: debian.org-upload.debian.org
Architecture: all
Depends: acl,
         python-apt,
         python-debian,
         python-psycopg2,
         python-sqlalchemy,
         python-rrdtool,
         python,
         libemail-sender-perl
Description: metapackage for upload-master host
 This package depends on everything that an upload master host (with
 queued and DEFERRED queue) needs.

Package: debian.org-release.debian.org
Architecture: all
Depends:
	colordiff,
	dose-distcheck,
	dose-builddebcheck,
	mutt,
	subversion,
	python2.7,
	python2.7-dev,
	python-dev,
	wdiff,
	python-pygresql,
	python-libxslt1,
	python-apt,
	python-debian,
	python-six,
	libapt-pkg-perl,
	libapt-pkg-dev,
	dpkg-dev,
	procmail,
	rrdtool,
	python3-apt,
	python3-debian,
	python3-six,
	python3-sphinx,
	python3-yaml,
	picosat,
	git-email,
	python-networkx,
	python-pygraphviz,
	python-yaml,
	postgresql-client,
	python-psycopg2,
	python3-psycopg2,
	python-sqlalchemy,
	libdbd-pg-perl,
	libsoap-lite-perl,
Description: metapackage for release
 This package depends on everything that is needed for a release.debian.org
 setup.

Package: debian.org-cdbuilder.debian.org
Architecture: all
Depends: bittornado,
	build-essential,
	debhelper,
	debian-cd,
	jigit,
	m4,
	tofrodos,
	qemu-system-arm,
	qemu-system-x86,
	qemu-utils,
	qemu-kvm,
	zip,
	zsync,
	fakeroot,
	dosfstools,
	xorriso,
	libsoap-lite-perl,
	mtools,
	syslinux-utils,
	libdbd-sqlite3-perl
Description: metapackage for cdbuilder
 This package depends on all the packages that are needed for a cdbuilder setup.

Package: debian.org-cdimage-search.debian.org
Architecture: all
Depends: libconfigreader-simple-perl, libdbd-sqlite3-perl
Description: metapackage for cdimage-search.debian.org
 This package depends on all the packages that are needed for the CD/DVD search CGI website

Package: debian.org-manpages.debian.org
Architecture: all
Depends: golang-go,
	mandoc
Description: metapackage for manpages
 This package depends on all the packages that are needed for manpages.d.o setup.

Package: debian.org-nm.debian.org
Architecture: all
Depends: libapache2-mod-wsgi-py3,
	mutt, mboxgrep,
	python,
	python-mako (>= 0.2.5), python-ldap,
	python-apt, python-beautifulsoup, python-debianbts, python-dns,
	python-dnspython, python-egenix-mxdatetime, python-egenix-mxtools,
	python-elementtree | python (>= 2.6), python-flup, python-libxml2,
	python-lxml, python-psycopg2, python-soappy,
	python-xapian,
	python-dateutil,
	python-debian,
	python-django (>= 1.8),
	python-django-south,
	python-debiancontributors,
	python-tz,
	python-markdown,
	python-lzma,
	python-requests (>= 2.0.0),
	python-djangorestframework,
	python3,
	python3-requests,
	python3-psycopg2,
	python3-django (>= 1.10),
	python3-ldap3,
	python3-apt,
	python3-xapian,
	python3-dateutil,
	python3-debian,
	python3-debiancontributors,
	python3-tz,
	python3-git,
	python3-markdown,
	python3-djangorestframework,
	procmail,
	rrdtool,
	moreutils,
	libjs-jquery,
	postgresql-client
Description: metapackage for nm.debian.org
 This package depends on all the packages that are needed for nm.debian.org setup.

Package: debian.org-backports.debian.org
Architecture: all
Depends: debian.org-ftpmaster.debian.org,
	libdbi-perl, libdbd-pg-perl
Description: metapackage for backports.debian.org
 This package depends on all the packages that are needed for backports.debian.org setup.

Package: debian.org-qa.debian.org
Architecture: all
Depends: db-util,
	devscripts (>= 2.15.9~),
	gnuplot-nox,
	wml,
	libapt-pkg-perl,
		libcgi-pm-perl,
		libdbd-pg-perl,
		libdate-manip-perl,
		libdpkg-perl,
		libmailtools-perl,
		libsoap-lite-perl,
		libtemplate-perl,
		libxml-simple-perl,
		libmail-sendmail-perl,
		libdate-calc-perl,
		libnet-ldap-perl,
		libjson-perl,
		libyaml-syck-perl,
	php,
	php-cgi,
	php-cli,
	php-ldap,
	php-pgsql,
	php-xml,
	php-dba,
	libapache2-mod-fcgid,
	python-apt, python-psycopg2, python-gpgme,
	python3, python3-apt, python3-psycopg2,
	postgresql-client,
	rename,
	rrdtool,
	raptor-utils | raptor2-utils,
	dose-distcheck, dose-builddebcheck, python3-yaml, python3-debian, python3-debianbts,
	brz,
	cvs,
	darcs,
	monotone,
	git,
	mercurial,
	subversion,
Description: metapackage for qa.debian.org
 This package depends on all the packages that are needed for qa.debian.org setup.

Package: debian.org-packages.qa.debian.org
Architecture: all
Depends: db4.7-util,
	libio-compress-perl,
	libio-compress-lzma-perl,
	libmailtools-perl,
	libmime-tools-perl,
	libmail-verp-perl,
	xsltproc,
	mhonarc,
	python,
	python-lxml,
	python-soappy,
	python-debian,
	python-yaml,
	python-zsi
Description: metapackage for packages.qa.debian.org
 This package depends on all the packages that are needed for packages.qa.debian.org setup.

Package: debian.org-tracker.debian.org
Architecture: all
Depends:
# Runtime
	daemon,
	libapache2-mod-wsgi-py3,
	python3-apt,
	python3-bs4,
	python3-debian,
	python3-debianbts (>= 2.6),
	python3-django (>= 1:1.11),
	python3-django-jsonfield,
	python3-gpg,
	python3-requests (>= 2),
	python3-yaml,
	python3-pyinotify,
	dpkg-dev,
# Database access
	python3-psycopg2,
	postgresql-client,
# Test suite and docs
	python3-sphinx,
	python3-sphinx-rtd-theme,
Description: metapackage for tracker.debian.org
 This package depends on all the packages that are needed for tracker.debian.org.

Package: debian.org-udd.debian.org
Architecture: all
Depends: curl,
	dpkg-dev,
	graphviz,
	libapt-pkg-dev,
	libcrypt-ssleay-perl,
	libcgi-simple-perl,
	libdbd-pg-perl,
	libdbi-perl,
	libdpkg-ruby,
	libio-stringy-perl,
	liblist-allutils-perl,
	liblist-moreutils-perl,
	libmime-tools-perl,
	libmldbm-perl,
	libparams-validate-perl,
	libperl-dev,
	libsafe-hole-perl,
	libtext-template-perl,
	libyaml-syck-perl,
	postgresql-autodoc,
	python-dateutil,
	python-debian,
	python-dev,
	python-launchpadlib,
	python-psycopg2,
	python-yaml,
	python3-psycopg2,
	python3-yaml,
	ruby-json,
	ruby-oj
Description: metapackage for udd.debian.org
 This package depends on all the packages that are needed for udd.debian.org setup.

Package: debian.org-snapshot.debian.org
Architecture: all
Depends:
	ruby,
	ruby-dbd-pg,
	python-yaml, python-psycopg2, python-lockfile, python-fuse, uuid-runtime,
	python-dev, gcc,
	python-pylons
Description: metapackage for snapshot.debian.org
 This package depends on all the packages that are needed for snapshot.debian.org setup.

Package: debian.org-women.debian.org
Architecture: all
Depends: wml
Description: metapackages for women.debian.org dependencies
 This package depends on everything that is needed for women.debian.org.


Package: debian.org-lintian.debian.org
Architecture: all
Depends:
# lintian's runtime dependencies
	binutils,
	binutils-multiarch,
	bzip2,
	diffstat,
	dpkg-dev,
	file,
	gettext,
	intltool-debian,
	libapt-pkg-perl,
	libarchive-zip-perl,
	libcapture-tiny-perl,
	libclass-accessor-perl,
	libclone-perl,
	libdigest-sha-perl,
	libdpkg-perl,
	libemail-valid-perl,
	libfile-basedir-perl,
	libio-async-perl,
	libhtml-parser-perl,
	libipc-run-perl,
	liblist-compare-perl,
	liblist-moreutils-perl,
	libparse-debianchangelog-perl,
	libpath-tiny-perl,
	libperlio-gzip-perl,
	libtext-levenshtein-perl,
	libtimedate-perl,
	libtry-tiny-perl,
	liburi-perl,
	libxml-simple-perl,
	libyaml-libyaml-perl,
	man-db,
	perl,
	t1utils,
	xz-utils,
# Needed for the "clean" + "rebuild-lintian.debian.org" targets (beyond the above)
	debhelper,
	docbook-utils,
	docbook-xml,
# Needed for the reporting framework (beyond the above)
	gnuplot-nox | gnuplot,
	libtext-template-perl,
	scour | python-scour (<< 0.36-2~),
Description: metapackages for lintian.debian.org dependencies
 This package depends on everything that is needed for lintian.debian.org.

Package: debian.org-popcon.debian.org
Architecture: all
Depends:
	libchart-perl,
	libcgi-pm-perl
Description: metapackages for popcon.debian.org dependencies
 This package depends on everything that is needed for popcon.debian.org.

Package: debian.org-dde.debian.net
Architecture: all
Depends: python-apt,
	python-debian,
	python-debianbts,
	python-feedparser,
	python-flup,
	python-nose,
	python-paste,
	python-psycopg2,
	python-pyrss2gen,
	python-soappy,
	python-werkzeug,
	python-xapian,
	python-yaml
Description: metapackages for dde.debian.net dependencies
 This package depends on everything that is needed for dde.debian.net.

Package: debian.org-debtags.debian.org
Architecture: all
Depends: python-apt,
	python3-apt,
	python-debian,
	python3-debian,
	python-django,
	python3-django,
	python-xapian (>= 1.2.7-1~bpo60+1),
	python3-xapian (>= 1.2.7-1~bpo60+1),
	python-psycopg2,
	python3-psycopg2,
	python-debiancontributors,
	python3-debiancontributors,
	python-six,
	python3-six,
	postgresql-client,
	g++,
	libapt-pkg-dev
Description: metapackages for debtags.debian.org dependencies
 This package depends on everything that is needed for debtags.debian.org.

Package: debian.org-sso.debian.org
Architecture: all
Depends: libapache2-mod-wsgi-py3,
	python3-requests,
	python3-django,
	python3-lockfile,
	python3-ldap3,
	python3-psycopg2,
	python3-openssl,
	gnutls-bin
Description: metapackages for sso.debian.org dependencies
 This package depends on everything that is needed for sso.debian.org.

Package: debian.org-lists.debian.org
Architecture: all
Depends: xapian-tools,
	xapian-omega,
	smartlist,
	procmail,
	gnuplot,
	rrdtool,
	libdpkg-perl,
	gettext,
	grepmail,
	libcompress-raw-zlib-perl,
	libgcrypt20-dev,
	libmoose-perl,
	libnet-oauth2-perl,
	carton,
	amavisd-new,
	amavis-lists,
	zoo,
	lha,
	p7zip,
	unrar-free,
	ripole,
	pfqueue,
	pyzor,
	razor,
	mrtgutils,
	pax,
	pwgen,
	rbldnsd,
	rng-tools,
	speedy-cgi-perl,
	ccze,
	pflogsumm
Description: metapackages for lists.debian.org dependencies
 This package depends on everything that is needed for lists.debian.org.

Package: debian.org-ddtp.debian.org
Architecture: all
Depends: libalgorithm-diff-xs-perl,
	libdbd-pg-perl,
	libdbi-perl,
	libmail-sender-perl,
	libmime-tools-perl,
	libtext-diff-perl,
	libtext-iconv-perl
	,
	gnuplot
	,
	libapache2-mod-wsgi,
	python-setuptools,
	python-psycopg2,
	python-django,
	python-sqlalchemy
	,
	tig,
	postgresql-client-9.1
Description: metapackages for ddtp.debian.org dependencies
 This package depends on everything that is needed for ddtp.debian.org.

Package: debian.org-i18n.debian.org
Architecture: all
Depends: gettext,
	po-debconf,
	libdpkg-perl,
	libfile-slurp-perl,
	liblocale-gettext-perl,
	libmailtools-perl,
	libsoap-lite-perl,
	libtext-iconv-perl,
	libtimedate-perl,
	libwww-perl,
	tig,
	rrdtool
Description: metapackages for i18n.debian.org dependencies
 This package depends on everything that is needed for i18n.debian.org.

Package: debian.org-l10n.debian.org
Architecture: all
Depends: gettext,
	po-debconf,
	libdpkg-perl,
	libfile-slurp-perl,
	liblocale-gettext-perl,
	libmailtools-perl,
	libsoap-lite-perl,
	libtext-iconv-perl,
	libtimedate-perl,
	libwww-perl,
	tig,
	rrdtool
Description: metapackages for l10n.debian.org dependencies
 This package depends on everything that is needed for l10n.debian.org.

Package: debian.org-piuparts-master.debian.org
Architecture: all
Depends: debhelper,
	asciidoc,
	git,
	xmlto,
	python-debianbts,
	apache2,
	ghostscript,
	golang-go (>= 2:1.7~5~bpo8+1),
	python-rpy2,
	r-base-dev,
	r-recommended,
	tango-icon-theme,
	screen,
	python-apt,
	python-distro-info,
	python-lzma,
	python-yaml,
	libsoap-lite-perl,
	dose-distcheck (>= 5.0.1-8~bpo8)
Description: metapackage for piuparts-master.debian.org
 This package depends on all the packages that are needed for the
 master in the piuparts.debian.org setup.

Package: debian.org-piuparts-slave.debian.org
Architecture: all
Depends: debhelper,
	asciidoc,
	git,
	xmlto,
	debootstrap,
	debsums,
	lsb-release,
	screen,
	python-debian,
	python-apt,
	python-distro-info,
	python-lzma,
	adequate (>= 0.15.1~bpo8)
Description: metapackage for piuparts-slave.debian.org
 This package depends on all the packages that are needed for the
 slaves in the piuparts.debian.org setup.

Package: debian.org-wiki.debian.org
Architecture: all
Depends: libapache2-mod-wsgi,
	libcgi-pm-perl,
	libjson-perl,
	python-moinmoin (>= 1.9.7-2~bpo7+1),
	python-launchpadlib,
	libsoap-lite-perl,
	python-docutils
Description: metapackage for wiki.debian.org
 This package depends on all the packages that are needed for the Debian wiki

Package: debian.org-vote.debian.org
Architecture: all
Depends: libparse-recdescent-perl,
	libmime-tools-perl,
	libnet-ldap-perl,
	libgnupg-interface-perl,
	libmail-gnupg-perl,
	graphviz
Description: metapackage for vote.debian.org
 This package depends on all the packages that are needed for DeVoteE

Package: debian.org-bits.debian.org
Architecture: all
Depends: pelican, python-feedparser
Description: metapackage for bits.debian.org
 This package depends on all the packages that are needed for the Debian
 blog bits.debian.org

Package: debian.org-d-i.debian.org
Architecture: all
Depends: gnuplot,
	libdate-manip-perl,
	libemail-mime-perl,
	libemail-sender-perl,
	libfile-slurp-perl,
	libgd-graph-perl,
	libhtml-template-perl,
	liblist-moreutils-perl,
	libsort-versions-perl,
	myrepos,
	gettext,
	graphviz,
	fonts-liberation,
	dose-distcheck,
	liburi-perl,
	aspell,
	libunicode-string-perl,
	libxml-libxml-perl,
# Taken from d-i's manual (src:installation-guide)'s build-deps:
	docbook,
	docbook-xml,
	docbook-xsl,
	xsltproc,
	gawk,
	libhtml-parser-perl,
	w3m,
	poxml,
	jadetex,
	openjade | openjade1.3,
	dblatex,
	docbook-dsssl,
	ghostscript,
	texlive-xetex,
	lmodern,
	texlive-lang-cyrillic,
	texlive-lang-czechslovak,
	texlive-lang-european,
	texlive-lang-french,
	texlive-lang-german,
	texlive-lang-greek,
	texlive-lang-italian,
	texlive-lang-korean,
	texlive-lang-other,
	texlive-lang-portuguese,
	texlive-lang-spanish,
	ko.tex-base,
	cm-super,
	fonts-wqy-microhei,
	fonts-vlgothic,
	fonts-noto-cjk,
	fonts-freefont-ttf,
	po-debconf
Description: metapackage for d-i.debian.org
 This package depends on all the packages that are needed for the
 d-i.debian.org setup.

Package: debian.org-codesearch.debian.org
Architecture: all
Depends: dpkg-dev,
	postgresql-client
Description: metapackage for codesearch.debian.org
 This package depends on all the packages that are needed for the codesearch
 setup.

Package: debian.org-dedup.debian.net
Architecture: all
Depends: curl,
	python,
	python-concurrent.futures,
	python-debian,
	python-imaging,
	python-jinja2,
	python-lzma,
	python-pkg-resources,
	python-werkzeug,
	python-yaml,
	python-sqlalchemy,
	python-ssdeep,
	python3,
	python3-debian,
	python3-jinja2,
	python3-pil,
	python3-pkg-resources,
	python3-werkzeug,
	python3-yaml,
	python3-sqlalchemy,
	python3-ssdeep,
	sqlite3
Description: metapackage for dedup.debian.net
 This package depends on all packages that are required for operating the
 Debian duplication detector.

Package: debian.org-wnpp-by-tags.debian.net
Architecture: all
Depends: debtags, python-debian, python-beautifulsoup
Description: metapackage for wnpp-by-tags.debian.net
 This package depends on all the packages that are needed for the wnpp-by-tags
 setup.

Package: debian.org-blends.debian.org
Architecture: all
Depends: python-genshi,
 python-markdown,
 python-docutils,
 python-debian,
 python-psutil,
 python-psycopg2,
 python-apt,
 python3-genshi,
 python3-markdown,
 python3-docutils,
 python3-debian,
 python3-psutil,
 python3-psycopg2,
 python3-apt,
 postgresql-client
Description: metapackage for blends.debian.org
 This package depends on all the packages that are needed for the
 blends.debian.org setup.

Package: debian.org-pet.debian.net
Architecture: all
Depends: python-apt,
	python-argparse,
	python-debian,
	python-debianbts,
	python-inotifyx,
	python-paste,
	python-psycopg2,
	python-pyramid,
	python-popcon,
	python-sqlalchemy,
	python-subversion
Description: metapackages for pet.debian.net dependencies
 This package depends on everything that is needed for the PET setup.

Package: debian.org-dsa.debian.org
Architecture: all
Depends: ikiwiki, libtext-wikicreole-perl, libtext-wikiformat-perl, libtext-multimarkdown-perl
Description: metapackages for dsa.debian.org master dependencies
 This package depends on everything that is needed for the service

Package: debian.org-dns.debian.org
Architecture: all
Depends: perl-doc,
	nagios-nrpe-plugin,
	sqlite3,
	libdata-compare-perl,
	libnet-dns-perl,
	libnet-dns-sec-perl,
	python-dnspython,
	python-yaml,
	letsencrypt.sh,
Description: metapackages for dns.debian.org master dependencies
 This package depends on everything that is needed for the service

Package: debian.org-nagios.debian.org
Architecture: all
Depends: icinga,
	nagios-plugins-contrib
Description: metapackages for nagios.debian.org master dependencies
 This package depends on everything that is needed for the service and
 also provides the dpkg-diverts for the REMOTE_USER hack

Package: debian.org-deriv.debian.net
Architecture: all
Depends: python,
	python3-apt,
	python3-atomicwrites,
	python3-cairosvg,
	python3-debian,
	python3-magic,
	python3-pil,
	python3-psycopg2,
	python3-requests,
	python3-simplejson,
	python3-yaml,
	libxml-feed-perl,
	apt-transport-https,
	dpkg-dev,
	patchutils,
	rsync,
	curl,
	make,
	lzip,
Homepage: https://wiki.debian.org/Derivatives/Integration#Patches
Description: metapackage for the derivatives census patch generation
 This package depends on everything that is needed for the service

Package: debian.org-httpredir.debian.org
Architecture: all
Depends: inoticoming,
	libanyevent-perl,
	libanyevent-http-perl,
	libev-perl,
	libtimedate-perl,
	libgeo-ip-perl,
	libwww-perl,
	liburi-perl,
	libplack-perl,
	starman
Description: metapackage for http-redirector dependencies
 This package depends on everything that is needed for an instance of
 the http-redirector.

Package: debian.org-search.debian.org
Architecture: all
Depends: xapian-omega
Description: metapackage for search.debian.org dependencies
 This package depends on everything that is needed for an instance of
 the www search engine (aka search.debian.org).

Package: debian.org-dgit.debian.org
Architecture: all
Depends: perl,
	libwww-perl,
	libdpkg-perl,
	git,
	devscripts,
	dpkg-dev,
	libdigest-sha-perl,
	liblist-moreutils-perl,
	libtext-iconv-perl,
	libtext-glob-perl,
	dput,
	curl,
	libjson-perl,
	gpgv,
	chiark-utils-bin,
	libdbd-sqlite3-perl,
	sqlite3
Description: metapackage for dgit.debian.org dependencies
 This package depends on everything that is needed for an instance of
 the dgit git server (aka dgit.debian.org).
 .
 (This is actually simply the dependencies of the dgit and
 dgit-infrastructure binary packages, but due to chicken-and-egg
 problems and for ease of maintenance we are running the dgit
 infrastructure out of a git tree.)

Package: debian.org-appstream.debian.org
Architecture: all
Depends: gdc,
	libappstream-dev,
	optipng,
	cmake,
	intltool,
	libglib2.0-dev,
	libxml2-dev,
	libgirepository1.0-dev,
	libxapian-dev,
	libyaml-dev,
	xmlto,
	gobject-introspection,
	libprotobuf-dev,
	protobuf-compiler,
	libgdk-pixbuf2.0-dev,
	libarchive-dev,
	librsvg2-dev,
	liblmdb-dev,
	libcairo2-dev
Description: metapackage for appstream.debian.org dependencies
 This package depends on everything that is needed for an instance of
 the DEP-11/AppStream metadata extractor.

Package: debian.org-debian-r.debian.org
Architecture: all
Depends: debian.org-ftpmaster.debian.org
Description: metapackage for debian-r.debian.org dependencies
 This package depends on everything that is needed for the debian-r
 archive software (which is generally everything that is required by
 ftpmaster.debian.org).

Package: debian.org-ftpmaster.debian-ports.org
Architecture: all
Depends: apt-utils,
	git,
	gnupg,
	python,
	python-debian,
	procmail,
	wget
Description: metapackage for debian-ports ftpmaster
 This package depends on everything that is needed for a debian-ports
 ftpmaster setup (using mini-dak).

Package: debian.org-bootstrap.debian.net
Architecture: all
Depends: curl,
	debian-ports-archive-keyring,
	gnuplot,
	dose-distcheck,
	apt-cudf,
	botch,
	python3-pydot,
	libjs-jquery,
	libjs-jquery-tablesorter,
	libgraph-easy-perl,
	libsoap-lite-perl,
	time,
Description: metapackage for bootstrap.debian.net
 This package depends on everything that is needed for the bootstrap
 analysis tools with results displayed on bootstrap.debian.net

Package: debian.org-mirror-master.debian.org
Architecture: all
Depends: sqlite3,
	python3-sqlalchemy,
	python3-dateutil,
	python3-jinja2,
	python3-bs4,
	python3-alembic (>= 0.8.7)
Description: metapackage for mirror-master.debian.org
 This package depends on everything that is needed for
 mirror-master.debian.org.

Package: debian.org-sources.debian.org
Architecture: all
Depends: apache2,
	libapache2-mod-fcgid,
	libjs-highlight.js,
	libjs-jquery,
	python-apt,
	python-debian,
	python-flask,
	python-flaskext.wtf,
	python-flup,
	python-magic,
	tango-icon-theme,
	diffstat,
	dpkg-dev,
	exuberant-ctags,
	python-lzma,
	python-matplotlib,
	python-psycopg2,
	python-sqlalchemy,
	sloccount,
Description: metapackage for sources.debian.org
 This package depends on everything that is needed for
 sources.debian.org (Debsources).

Package: debian.org-veyepar.debian.org
Architecture: all
Depends: apache2,
	inkscape,
	bs1770gain,
	ffmpeg,
	git,
	postgresql-client,
	python3-psycopg2,
	melt,
	python3-pyrss2gen,
	python3-virtualenv,
	libapache2-mod-wsgi-py3
Description: metapackage for veyepar.debian.org
 This package depends on everything (hopefully) that is needed for
 veyepar.debian.org (one option for DebConf video review system)

Package: debian.org-sreview.debian.org
Architecture: all
Depends: apache2,
	bs1770gain,
	ffmpeg,
	fonts-noto-cjk,
	fonts-noto-cjk-extra,
	git,
	gridengine-client,
	gridengine-exec,
	gstreamer1.0-plugins-base-apps,
	gstreamer1.0-plugins-good,
	gstreamer1.0-tools,
	inkscape,
	kgb-client,
	libemail-sender-perl,
	libemail-simple-perl,
	libglib-object-introspection-perl,
	libglib-perl,
	libjson-xs-perl,
	libmojo-pg-perl,
	libmojolicious-perl,
	libmoose-perl,
	libxml-rss-perl,
	libyaml-perl,
	postgresql-client,
	sox
Recommends:
	gridengine-master
Description: metapackage for sreview.debian.{org,net}
 This package depends on everything (hopefully) that is needed for
 sreview.debian.net, to become sreview.debian.org at some point (other option
 for DebConf video review system)

Package: debian.org-wafer.debconf.org
Architecture: all
Depends:
	memcached,
	nodejs-legacy,
	python3,
	python3-diff-match-patch,
	python3-django (>= 1:1.11),
	python3-django-jsonfield,
	python3-django-nose,
	python3-django-reversion,
	python3-djangorestframework,
	python3-libravatar,
	python3-markdown,
	python3-pil,
	python3-psycopg2,
	python3-requests,
	python3-tz,
	python3-venv,
	python3-wheel,
	python3-yaml,
	libjs-jquery,
Description: metapackage for wafer-based debconf.org websites
 This package depends on everything (hopefully) that is needed for
 wafertest.debconf.org, and similar.

Package: debian.org-btslinks.debian.org
Architecture: all
Depends:
        python,
        python-bs4,
        python-debianbts,
        python3-debianbts,
        python-lxml,
        python-ldap,
        python-rrdtool,
        python-soappy,
        python-yaml,
        rrdtool,
Description: metapackage for bts-link
 This package depends on everything (hopefully) that is needed for
 running bts-link.
